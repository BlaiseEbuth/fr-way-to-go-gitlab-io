import React from 'react';
import {Link} from 'react-router-dom';
import {first, includes} from 'lodash';
import {useAsync} from 'react-async';

import content from '../content';
import categorizedPages from '../categorizedPages';

export default function TopLevelNav({page, locale}) {
    const {data: categories} = useAsync({
        promiseFn: content,
        watch: `./${locale}/categories.yaml`,
        path: `./${locale}/categories.yaml`,
    });

    if (!categories) {
        return null;
    }

    return <ul id="tl-nav">
        {categorizedPages.map(
            category =>
                <li key={category.name} className={includes(category.pages, page)?'active-tl':''}>
                    <Link to={`/${locale}/${first(category.pages)}`}>
                        {categories[category.name]}
                    </Link>
                </li>
        )}
    </ul>;
}
