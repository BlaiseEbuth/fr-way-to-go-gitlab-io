## Cattura le pietre in atari!

Normalmente, vengono usate tavole 19x19, ma le più piccole 9x9 sono
generalmente raccomandate per i principianti.

Da nero, prova a catturare la pietra bianca.

Board::capture1

---

Due pietre bianche sono in atari. Catturale con una sola mossa!

Board::capture2

---

Anche se ci sono altre pietre nelle vicinanze, può applicare lo stesso principio
per catturare.

Trova la mossa per catturare bianco!

Board::capture3

---

Ripeti i problemi sopra riportati finchè non li avrai compresi appieno.

Se lo hai fatto, io ti concedo il grado di **50 Kyu** - il livello più basso del mio personale
sistema di ranking :)

## Riguardo al sistema di ranking del Go

Nel mondo del Go amatoriale, 30 Kyu è di solito il livello più basso. Più è piccolo il grado kyu e
più forte è il giocatore.

1 dan viene dopo 1 kyu. Da quel punto in poi maggiore è il dan e più forte è il giocatore.

I giocatori dan sono considerati parecchio forti.
