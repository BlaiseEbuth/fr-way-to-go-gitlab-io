## Avvicinarsi al fine gioco

Quando il gioco si avvicina alla fine, sia il territorio nero che quello bianco
sono decisi, tutto quello che resta da fare è delimitare chiaramente le linee di confine.

A questo punto della partita, devi cercare di spingere il confine
allargando il tuo territorio il più possibile mentre riduci quello del tuo avversario.

---

Nero occupa il lato sinistro e bianco controlla il destro. Tuttavia, ci sono ancora
alcune zone dove il confine non è definito.

Qual è il punto dove puoi ottenere maggior profitto?

Board::yose-1

---

Nero ha il lato sinistro e bianco il destro. La partita è quasi finita, ma sta attento
fino alla fine!

Bianco ha appena giocato la pietra segnata. Se la ignori, il tuo prezioso territorio
sarà rovinato.

Board::yose-2
