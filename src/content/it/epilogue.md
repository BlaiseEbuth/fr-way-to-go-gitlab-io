### Epilogo

La strada interattiva per il Go (The Interactive Way To Go) è ora finita.

Spero che ora tu possa apprezzare il Go come un gioco interessante ed
eccitante. Ora, cosa puoi fare per continuare a giocare a Go e diventare
più forte?

1. Fare partite

Il Go è un gioco. Divertiti giocando più che puoi. Imparerai molto già dal
semplice fatto di far pratica. Se non hai nessuno con cui giocare, allora
vai sui server di Go online.

Ci sono vari server su internet.

* [online-go.com][OGS] - Un web server moderno con una sempre più grande
  comunità in continua crescità. Si possono fare sia partite in tempo reale
  che per corrispondenza. (HTML5 client)
* [KGS Go Server][KGS] - Spazio semplice e libero dove giocare a go.
  (Java, JS, Android clients)
* [Pandanet][IGS] - Uno dei server più grandi. Ha migliaia di giocatori
  connessi a qualunque ora. (Client for all platforms PC and mobile)
* [Color Go Server][color]

Puoi anche giocare contro programmi di computer. Ci sono molti prodotti in giro.
Sui server è possibile giocare contro dei bot di vari livello, sia molto deboli,
adatti a principianti, sia fortissimi in grado di giocare contro i professionisti.
Igowin è uno dei migliori programmi per principianti. E' gratuito, ma solo per Windows.

* [COSUMI][]
* [KaTrain][katrain]

2. Osservare partite

Sui server di Go, è anche possibile osservare le altre persone che giocano. Puoi
imparare molto osservando le partite di giocatori forti. Imparare dalle partite dei
professionisti è la via più veloce per diventare più forte.

3. Risolvere problemi

I problemi di vita e morte (chiamati tsumego) sono problemi su come uccidere o far
sopravvivere un gruppo di pietre in una situazione locale sul goban. Ci sono
molte prezione tecniche da apprendere in quelle situazioni.

### Fine

[COSUMI]: https://www.cosumi.net/
[IGS]: https://pandanet-igs.com/
[KGS]: https://gokgs.com
[OGS]: https://www.online-go.com
[color]: https://colorgoserver.com/
[iwtg]: http://playgo.to/iwtg/en/
[katrain]: https://github.com/sanderland/katrain
[puyo]: https://puyogo.app/
[toyo]: http://www.toyo-igo.com/
