
Arrivato a 40 kyu, dovresti essere in grado di risolvere problemi più difficili.

Buona fortuna!

Cattura le 3 pietre bianche.

Board::cranes-nest

Questa è una forma famosa chiamata "Nido della gru". Può essere davvero eccitante se
riesci a giocarlo in partita :)

---

Le pietre nere hanno circondato o sono state circondate?

Cattura bianco prima di essere catturato tu stesso!

Board::capture-race

Nota che questa è una gara di cattura!
