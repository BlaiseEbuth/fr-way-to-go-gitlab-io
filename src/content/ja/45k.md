## シチョウという事情

シチョウと呼ばれる形を覚えます。

白石を取って下さい。
白石は逃げ出しますがとにかくアタリを打ち続けて下さい。

Board::ladder-1

うまく白をつかまえることができましたか？

このように階段（かいだん）のように逃げていく形を**シチョウ**と言います。
白は逃げれば逃げるほど被害（ひがい）を大きくしているだけでした。

---

さて、またシチョウの図ですが今度は黒が逃げる番です。
しかし上の場合の白とちがって逃げ切ることができます。

Board::ladder-2

うまく白のアタリ攻勢（こうせい）から逃げることができましたか？

このように逃げ道の途中に味方の石があると、それにつかまることができるので逃げ切ることができます。
このような石をシチョウアタリといいます。

さて、無理（むり）して追いかけた白はこの後どうなるでしょうか？
とても悲惨（ひさん）なことになります。

シチョウは逃げる方も追いかける方もよく先を読んでから行動して下さい。
まちがえるとボロボロになります。

格言 - 「シチョウ知らずに碁を打つな」

## 囲碁の格言

[格言][go-proverbs]は囲碁の知見を簡潔な言葉で表現したものです。
対局や解説の際に、このような格言が発せられることがよくあります。

[go-proverbs]: http://senseis.xmp.net/?GoProverbs
