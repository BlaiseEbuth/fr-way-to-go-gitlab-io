# インタラクティブ囲碁入門

ここは、他の囲碁の入門書と違って説明を読むだけではなく、実際に打ちながら囲碁のルールと基本的なテクニックを学ぶことができます。
パズル感覚でできるので、きっと楽しみながら囲碁をおぼえることができると思います。

### 支援方法

* 異なる言語についての知識がある方は、[issue on GitLab][gitlab]までご連絡ください。プログラミングについての知識は必要ありません。
* バグを見つけた方はこちらまで [issue][gitlab]!

### 翻訳者

以下の翻訳者に特に感謝します！

::Translators::

### 謝辞

* オリジナルは森博嗣氏によるサイトです。
* 森博嗣がコンテンツとコードの両方を手がけ、1997年に公開された初代 [インタラクティブ囲碁入門][iwtg] は、何千人もの囲碁入門者を増やしました。
* 森氏の許可を得て、[duckpunch][]が現代の手法でコードを書き直したのものを2019年にリリースし、オリジナルの内容をほぼそのまま残しています。

[duckpunch]: https://duckpunch.org
[gitlab]: https://gitlab.com/way-to-go/way-to-go.gitlab.io
[go-game]: https://en.wikipedia.org/wiki/Go_(game)
[iwtg]: https://web.archive.org/web/20210831172631/http://playgo.to/iwtg/en/
