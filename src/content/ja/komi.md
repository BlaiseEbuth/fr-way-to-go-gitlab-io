### コミについて

通常（つうじょう）の対局では黒が必ず初手（しょて）を打つ決まりになっています。 陣地争いの先陣（せんじん）をきれるので黒がやや有利（ゆうり）となります。
この不公平（ふこうへい）さを解消（かいしょう）するため、最後に地を数える時に、白にボーナスポイントが与えられます。
このことを「コミ」と呼びます。

このボーナスは普通６目（もく）半です。つまり初手（しょて）の価値（かち）は地に換算（かんさん）して6.5目分というわけです。
「半（はん）」というのは引き分けをさけるための工夫（くふう）です。

たとえば、地を数えた結果、黒地が白地より７目多ければ7-6.5=0.5なので黒の半目勝ちで、黒地が６目多ければ、6-6.5=-0.5なので白の半目勝ちというわけです。

ところで昔の碁にはコミはありませんでした。 しかし黒の勝率（しょうりつ）が高かったのでコミが導入（どうにゅう）されました。
コミの数は時代とともにかわっており、コミ＝４目半の時代もありましたがそれでも黒の勝率が高かったので、現在はコミ＝６目半が主流となっています。（場合によってはコミが５目半のこともあります。）

コミのおかげで黒と白は互角（ごかく）となり、引き分けもほとんどなく、囲碁を優（すぐ）れたゲームにしています。



---

### 置碁（おきご）について

実力に差がある場合、実力の劣（おと）っているほうがはじめに盤上にいくつかの石をおいてから対局します。
このことを置碁（おきご）といいます。

ハンディキャップのある置碁では、下手（したて：弱い方）が常に黒を持ち、上手（うわて：強い方）が必ず白を持ちます。

下手は実力の差だけ黒石を置いてから対局開始します。したがって置碁では白が初手を打ちます。

置石（おきいし）の数は段級位（だんきゅうい）の差と同じにします。
たとえば下手が５級で上手が２級だとすれば３個置きます。 これを３子（し）局と呼びます。

置石の分だけ、黒は地のとりあい、戦いなどあらゆる面で有利になります。 置石１つについて地に換算（かんさん）すると約１０目分の価値があるとされています。

ですから、ある人とハンデなしで対局して５０目負けたとしたら、 その人とは５子の置碁でやるとちょうどよいということになります。

置石をどこに置くかはだいたい決まっており、19路盤の場合は以下のようになっています。

---

９子局

Board::komi0

---

６子局

Board::komi1

---

５子局

Board::komi2

---

４子局

Board::komi3

---

いずれの場合も、星とよばれる盤上の黒い小さな印（しるし）のある場所に置くことになっています。
９子以上の実力差がある場合でも、さらに置石を増やすこともできます。
「自由置碁（じゆうおきご）」といい、上のように星に置かず、好きなところに置いてもよいというやりかたもあります。

このように、囲碁はどんなに実力差があっても、置碁にすれば勝つチャンスを対等にして対局を楽しむことができます。

なお、置碁では双方（そうほう）の実力が大きく違うので、微妙（びみょう）な差を打ち消すためのコミは普通使いません。
そのため引き分け（「ジゴ」といいます）になることもあります。