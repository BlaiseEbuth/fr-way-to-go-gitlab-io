### Epilogue

The Interactive Way To Go is now over.

I hope that you can now appreciate that Go is really an interesting and an
exciting game.  Now, what do you have to do to keep playing Go and get
stronger?

1. Play Games

Go is a game. Have fun by playing as much as possible. You will learn a lot
from just playing.  If there is nobody to play with, then go to the Internet Go
servers.

We have several Go servers in the net.

* [online-go.com][OGS] - A modern web server with a larger and growing
  community. Has both live and correspondence games. (HTML5 client)
* [KGS Go Server][KGS] - Free and easy place to play go. (Java, JS, Android
  clients)
* [Pandanet][IGS] - One of the largest go servers. Normally several thousand
  players on at any time. (Client for all platforms PC and mobile)
* [Color Go Server][color]

You can also play with computer programs. Several products are out there and
with the Artificial Intelligence advancement in the last few years they became
very strong, but they are not trivial to setup and configure.

* [COSUMI][]
* [KaTrain][katrain]

2. Observe games

On the Go servers, you can watch other people playing Go. You can learn a lot
by observing strong players' games.  Learning from professional players' games
is the fastest way to get stronger.

3. Solve problems

The Life and Death problems are problems on how to kill or survive a group of
stones locally on the board. There are many valuable techniques there.

### The End

[COSUMI]: https://www.cosumi.net/
[IGS]: https://pandanet-igs.com/
[KGS]: https://gokgs.com
[OGS]: https://www.online-go.com
[color]: https://colorgoserver.com/
[iwtg]: http://playgo.to/iwtg/en/
[katrain]: https://github.com/sanderland/katrain
[puyo]: https://puyogo.app/
[toyo]: http://www.toyo-igo.com/
